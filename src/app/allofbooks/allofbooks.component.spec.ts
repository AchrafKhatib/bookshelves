import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllofbooksComponent } from './allofbooks.component';

describe('AllofbooksComponent', () => {
  let component: AllofbooksComponent;
  let fixture: ComponentFixture<AllofbooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllofbooksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllofbooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
