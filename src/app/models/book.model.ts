export class Book {
  photo: string;
  constructor(
    public title: string,
    public author: string,
    public taken: boolean = false,
    public holderBy: number = 0
  ) {}
}
