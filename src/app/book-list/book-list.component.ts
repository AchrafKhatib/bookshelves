import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { Subscription } from "rxjs";
import { Book } from "../models/book.model";
import { BooksService } from "../services/books.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-book-list",
  templateUrl: "./book-list.component.html",
  styleUrls: ["./book-list.component.css"]
})
export class BookListComponent implements OnInit, OnDestroy {
  books: Book[];
  bookSubscription: Subscription;
  @Input() showBtn: boolean = true;
  @Input() showremove: boolean = true;
  constructor(private booksService: BooksService, private router: Router) {}

  ngOnInit() {
    this.bookSubscription = this.booksService.booksSubject.subscribe(
      (books: Book[]) => {
        this.books = books;
      }
    );
    this.booksService.getBooks();
    this.booksService.emitBooks();
  }
  onNewBook() {
    this.router.navigate(["/books", "new"]);
  }
  onDeleteBook(book: Book) {
    this.booksService.removeBook(book);
  }
  onViewBook(id: number) {
    this.router.navigate(["/books", "view", id]);
  }
  ngOnDestroy() {
    this.bookSubscription.unsubscribe();
  }
}
