import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SignupComponent } from "./auth/signup/signup.component";
import { SigninComponent } from "./auth/signin/signin.component";
import { BookListComponent } from "./book-list/book-list.component";
import { SingleBookComponent } from "./book-list/single-book/single-book.component";
import { BookFormComponent } from "./book-list/book-form/book-form.component";
import { HeaderComponent } from "./header/header.component";
import { AuthService } from "./services/auth.service";
import { BooksService } from "./services/books.service";
import { AuthGuardService } from "./services/auth-guard.service";
import { HoldersService } from "./services/holders.service";
import { HolderComponent } from './holder/holder.component';
import { AllofbooksComponent } from './allofbooks/allofbooks.component';
import { ReservedbooksComponent } from './reservedbooks/reservedbooks.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    BookListComponent,
    SingleBookComponent,
    BookFormComponent,
    HeaderComponent,
    HolderComponent,
    AllofbooksComponent,
    ReservedbooksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthService, BooksService, AuthGuardService, HoldersService],
  bootstrap: [AppComponent]
})
export class AppModule {}
