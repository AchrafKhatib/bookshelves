import { Injectable } from "@angular/core";
import { Holder } from "../models/holder.model";

@Injectable({
  providedIn: "root"
})
export class HoldersService {
  holders: Holder[] = [
    { id: 1, name: "Alex" },
    { id: 2, name: "Joe" },
    { id: 3, name: "Jessica" }
  ];

  constructor() {}

  getHolders() {
    return this.holders;
  }
}
