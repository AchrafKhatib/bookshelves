import { Component, OnInit } from "@angular/core";
import { HoldersService } from "../services/holders.service";
import { Holder } from "../models/holder.model";

@Component({
  selector: "app-holder",
  templateUrl: "./holder.component.html",
  styleUrls: ["./holder.component.css"]
})
export class HolderComponent implements OnInit {
  constructor(private holdersService: HoldersService) {}

  holders: Holder[];

  ngOnInit() {
    this.holders = this.holdersService.getHolders();
  }
}
