import { Component } from "@angular/core";
import * as firebase from "firebase";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  constructor() {
    // Your web app's Firebase configuration
    var firebaseConfig = {
      apiKey: "AIzaSyDWkcW4jm-qnfBIwMaz1Y2bi-E_DR1G2rk",
      authDomain: "bookshelves-6cfa7.firebaseapp.com",
      databaseURL: "https://bookshelves-6cfa7.firebaseio.com/",
      projectId: "bookshelves-6cfa7",
      storageBucket: "gs://bookshelves-6cfa7.appspot.com",
      messagingSenderId: "379945057970",
      appId: "1:379945057970:web:03623b5bf9d0d79e"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
}
