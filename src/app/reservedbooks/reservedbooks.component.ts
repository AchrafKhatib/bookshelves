import { Component, OnInit } from "@angular/core";
import { BooksService } from "../services/books.service";
import { Book } from "../models/book.model";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-reservedbooks",
  templateUrl: "./reservedbooks.component.html",
  styleUrls: ["./reservedbooks.component.css"]
})
export class ReservedbooksComponent implements OnInit {
  books: Book[];
  id: string;
  constructor(
    private bookService: BooksService,
    private route: ActivatedRoute
  ) {
    this.route.paramMap.subscribe(parametre => (this.id = parametre.get("id")));
  }

  ngOnInit() {
    this.books = this.bookService.books;
  }
  onClick(book) {
    book.taken = false;
    book.holderBy = null;
    this.bookService.saveBooks();
  }
}
