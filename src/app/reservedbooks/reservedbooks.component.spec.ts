import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservedbooksComponent } from './reservedbooks.component';

describe('ReservedbooksComponent', () => {
  let component: ReservedbooksComponent;
  let fixture: ComponentFixture<ReservedbooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservedbooksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservedbooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
